using System;
using System.Globalization;
using System.Windows.Data;

namespace PrimeSearch.UI
{
    [ValueConversion(typeof(bool), typeof(bool))]
    public class BoolInversionConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(targetType != typeof(bool))
                throw new NotSupportedException();
            bool x = (bool) value;
            return !x;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(bool))
                throw new NotSupportedException();

            bool x = (bool) value;
            return !x;
        }
    }
}